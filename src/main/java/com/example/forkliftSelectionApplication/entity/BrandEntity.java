package com.example.forkliftSelectionApplication.entity;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "fl_brand")
@Data
public class BrandEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "brand_code")
	private Long brandCode;

	@ManyToOne
	@JoinColumn(name = "manifacturer_code_fk", referencedColumnName = "manifacturer_code")
	private ManufacturerEntity manufacturerEntity;

	@Column(name = "brand_name")
	private String brandName;

	@Column(name = "image_url")
	private String imageUrl;

	@Column(name = "created")
	private LocalDateTime created;

	@Column(name = "status")
	private String status;
}
