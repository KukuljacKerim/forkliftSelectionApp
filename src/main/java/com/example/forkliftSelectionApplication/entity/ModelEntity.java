package com.example.forkliftSelectionApplication.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "fl_model")
@Data
public class ModelEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "model_code")
	private Long modelCode;

	@ManyToOne
	@JoinColumn(name = "brand_code_fk", referencedColumnName = "brand_code")
	private BrandEntity brandEntity;

	@ManyToOne
	@JoinColumn(name = "drive_type_code_fk", referencedColumnName = "drive_type_code")
	private DriveTypeEntity driveTypeEntity;

	@ManyToOne
	@JoinColumn(name = "warehouse_code_fk", referencedColumnName = "warehouse_code")
	private WarehouseTypeEntity warehouseTypeEntity;

	@Column(name = "model_name")
	private String modelName;

	@Column(name = "image_url")
	private String imageUrl;

	@Column(name = "status")
	private String status;

}
