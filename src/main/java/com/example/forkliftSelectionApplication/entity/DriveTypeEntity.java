package com.example.forkliftSelectionApplication.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;


@Entity
@Table(name = "fl_drive_type")
@Data
public class DriveTypeEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "drive_type_code")
	private Long driveTypeCode;

	@Column(name = "drive_type_name")
	private String driveTypeName;
	
	@Column(name = "status")
	private String status;

}
