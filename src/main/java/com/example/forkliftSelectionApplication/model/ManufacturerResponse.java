package com.example.forkliftSelectionApplication.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class ManufacturerResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long manufacturerCode;
	private String name;
}