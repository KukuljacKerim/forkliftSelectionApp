package com.example.forkliftSelectionApplication.rest.manufcaturer;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.forkliftSelectionApplication.entity.ManufacturerEntity;
import com.example.forkliftSelectionApplication.model.BrandResponse;
import com.example.forkliftSelectionApplication.model.ManufacturerResponse;
import com.example.forkliftSelectionApplication.service.ManufacturerService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/manufacturers")

public class ManufacturerRestService {
	@Autowired
	private ManufacturerService manufacturerService;

	@Operation(summary = "Find all")
	@GetMapping(value = "get-all")
	public List<ManufacturerEntity> find() {
		return manufacturerService.findAll();
	}
	@Operation(summary = "Find by id")
	@GetMapping(value = "{id}")
	public Optional<ManufacturerResponse> findBrandById(@PathVariable final Long id) {
		return manufacturerService.findById(id);
	}
}
