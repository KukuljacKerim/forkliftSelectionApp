package com.example.forkliftSelectionApplication.service;

import java.util.List;
import java.util.Optional;

import com.example.forkliftSelectionApplication.entity.ManufacturerEntity;
import com.example.forkliftSelectionApplication.model.BrandResponse;
import com.example.forkliftSelectionApplication.model.ManufacturerResponse;

public interface ManufacturerService {
	List<ManufacturerEntity> findAll();

	Optional<ManufacturerResponse> findById(Long id);
}
