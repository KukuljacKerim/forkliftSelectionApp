package com.example.forkliftSelectionApplication.implementation;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.forkliftSelectionApplication.dao.ManufacturerDAO;
import com.example.forkliftSelectionApplication.entity.BrandEntity;
import com.example.forkliftSelectionApplication.entity.ManufacturerEntity;
import com.example.forkliftSelectionApplication.model.BrandResponse;
import com.example.forkliftSelectionApplication.model.ManufacturerResponse;
import com.example.forkliftSelectionApplication.service.ManufacturerService;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@Service
@RequiredArgsConstructor
public class ManufacutererServiceImpl implements ManufacturerService {
	@Autowired
	private ManufacturerDAO manufacturerDAO;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<ManufacturerEntity> findAll() {
		System.out.println("Find All " + manufacturerDAO.findAll());
		return manufacturerDAO.findAll();
	}

	@Override
	public Optional<ManufacturerResponse> findById(Long id) {
		Optional<ManufacturerEntity> manufacturerEntity = manufacturerDAO.findById(id);
		ManufacturerResponse manufacturerResponse = modelMapper.map(manufacturerEntity.get(), ManufacturerResponse.class);
		return Optional.ofNullable(manufacturerResponse);
	}


}
